package acad276.thoman.blair.a2;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    ImageView imageAmerican;
    ImageView imageCuban;
    ImageView imageChinese;
    ImageView imageFrench;
    ImageView imageGerman;
    ImageView imageEthiopian;
    ImageView[] typesOfFood;
    TextView[] foodTexts;


    int americanCount;
    int cubanCount;
    int chineseCount;
    int frenchCount;
    int germanCount;
    int ethiopianCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        String[] theURLs = res.getStringArray(R.array.theURLs);
        String[] imageName = res.getStringArray(R.array.imageName);


        typesOfFood = new ImageView[6];
        typesOfFood[0] = (ImageView) findViewById(R.id.imageAmerican);
        typesOfFood[1] = (ImageView) findViewById(R.id.imageChinese);
        typesOfFood[2] = (ImageView) findViewById(R.id.imageCuban);
        typesOfFood[3] = (ImageView) findViewById(R.id.imageEthiopian);
        typesOfFood[4] = (ImageView) findViewById(R.id.imageFrench);
        typesOfFood[5] = (ImageView) findViewById(R.id.imageGerman);

        foodTexts = new TextView[6];
        foodTexts[0] = (TextView) findViewById(R.id.textAmerican);
        foodTexts[1] = (TextView) findViewById(R.id.textChinese);
        foodTexts[2] = (TextView) findViewById(R.id.textCuban);
        foodTexts[3] = (TextView) findViewById(R.id.textEthiopian);
        foodTexts[4] = (TextView) findViewById(R.id.textFrench);
        foodTexts[5] = (TextView) findViewById(R.id.textGerman);


        for (int i = 0; i<theURLs.length; i++){
            Glide.with(getApplicationContext()).
                    load(theURLs[i]).
                    into(typesOfFood[i]);
        }

        for (int i = 0; i<imageName.length; i++){
            foodTexts[i].setText(imageName[i]);
        }


        typesOfFood[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                americanCount = americanCount + 1;
                Toast.makeText(getApplicationContext(), "You clicked American " + americanCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        typesOfFood[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chineseCount = chineseCount + 1;
                Toast.makeText(getApplicationContext(), "You clicked Chinese " + chineseCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        typesOfFood[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cubanCount = cubanCount + 1;
                Toast.makeText(getApplicationContext(), "You clicked Cuban " + cubanCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        typesOfFood[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ethiopianCount = ethiopianCount + 1;
                Toast.makeText(getApplicationContext(), "You clicked Ethiopian " + ethiopianCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        typesOfFood[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frenchCount = frenchCount + 1;
                Toast.makeText(getApplicationContext(), "You clicked French " + frenchCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        typesOfFood[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                germanCount = ethiopianCount+1;
                Toast.makeText(getApplicationContext(), "You clicked German " + germanCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
