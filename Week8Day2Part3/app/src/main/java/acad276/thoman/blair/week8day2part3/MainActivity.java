package acad276.thoman.blair.week8day2part3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, R.string.message, Toast.LENGTH_SHORT).show();


    }

    public void buttonClickMethod(View v){
        Toast.makeText(this, R.string.message, Toast.LENGTH_LONG).show();

    }
}
