package acad276.thoman.blair.custommm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MySimpleArrayAdapter(Context context1, String[] values) {
        super(context1, R.layout.activity_main, values);
        this.context = context1;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_main, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        TextView textView1 = (TextView) rowView.findViewById(R.id.secondLabel);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        if (values.length == 10)
            textView.setText(values[position]);
        if (values.length == 9)
            textView1.setText(values[position]);


        return rowView;
    }
}