package acad276.thoman.blair.custommm;

import android.app.ListActivity;
import android.os.Bundle;

public class MainActivity1 extends ListActivity {
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2" };
        MySimpleArrayAdapter1 adapter = new MySimpleArrayAdapter1(getApplicationContext(), values);
        setListAdapter(adapter);

    }

}