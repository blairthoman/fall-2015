package acad276.thoman.blair.a1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class onCreate extends AppCompatActivity {


    Button buttonDog;
    Button buttonCat;
    int catCount = 0;
    int dogCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_create);

        buttonCat = (Button)findViewById(R.id.buttonCat);
        buttonDog = (Button)findViewById(R.id.buttonDog);

        buttonCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catCount = catCount + 1;
                Toast.makeText(getApplicationContext(), "You pet a cat " + catCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });

        buttonDog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dogCount = dogCount + 1;
                Toast.makeText(getApplicationContext(), "You pet a dog " + dogCount + " times!", Toast.LENGTH_SHORT).show();
            }
        });





    }
}
