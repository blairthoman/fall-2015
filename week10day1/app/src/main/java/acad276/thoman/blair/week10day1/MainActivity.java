package acad276.thoman.blair.week10day1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    ImageView imageStill;
    ImageView imageAnimated;
    Button buttonStill;
    Button buttonAnimated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageStill = (ImageView) findViewById(R.id.imageStill);
        imageAnimated = (ImageView) findViewById(R.id.imageAnimated);
        buttonStill = (Button) findViewById(R.id.buttonStill);
        buttonAnimated = (Button)findViewById(R.id.buttonAnimated);
        Glide.with(getApplicationContext()).
                load("http://tinyurl.com/pufesur").
                placeholder(R.drawable.empty).
                into(imageStill);
        buttonStill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Loading...", Toast.LENGTH_SHORT).show();



                Toast.makeText(getApplicationContext(), "Image Finished", Toast.LENGTH_SHORT).show();

            }
        });

        buttonAnimated.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Glide.with(getApplicationContext()).
                        load("http://tinyurl.com/pcpemta").
                        asGif().
                        placeholder(R.drawable.placeholder).
                        into(imageAnimated);
                Toast.makeText(getApplicationContext(), "Image Finished", Toast.LENGTH_SHORT).show();

            }

        });


    }
}
